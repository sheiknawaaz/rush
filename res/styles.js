import React, { Component } from "react";
import { StyleSheet } from "react-native";
import * as GlobalColors from "./colors";

module.exports = StyleSheet.create({
  card: {
    flex: 1,
    backgroundColor: "white",
    padding: 0
  },
  safeAreaDark: {
    flex: 1,
    backgroundColor: GlobalColors.PRIMARY
  },
  safeAreaLight: {
    flex: 1,
    backgroundColor: "transparent"
  }
});
