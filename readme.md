Travelling Salesman App
=======================

This app uses a brute force approach to solving the travelling salesperson problem. It calculates the distances (and travel times) of each combination of geo locations using the Google Maps distance api. For each permutation of routes (with first address being start and end point), it then calculates the total weight (sum of distance between each pair) of each route. The optimal route will be the route with the lowest weight.

Each addition of an address will schedule a notification in 5 minutes.
The IOS version has Rush-Development and Rush-Production schemes.
The Android version has dev and prod product flavors. 
The app will display the current mode (Production or Development) in a snackbar when app first starts up. 

![Simulator Screen Shot - iPhone X - 2018-11-21 at 19.58.55.png](https://bitbucket.org/repo/4pGgEAR/images/3483783816-Simulator%20Screen%20Shot%20-%20iPhone%20X%20-%202018-11-21%20at%2019.58.55.png)
![Simulator Screen Shot - iPhone X - 2018-11-21 at 19.58.21.png](https://bitbucket.org/repo/4pGgEAR/images/2231934298-Simulator%20Screen%20Shot%20-%20iPhone%20X%20-%202018-11-21%20at%2019.58.21.png)
![Simulator Screen Shot - iPhone X - 2018-11-21 at 19.59.13.png](https://bitbucket.org/repo/4pGgEAR/images/2801022811-Simulator%20Screen%20Shot%20-%20iPhone%20X%20-%202018-11-21%20at%2019.59.13.png)
![Simulator Screen Shot - iPhone X - 2018-11-22 at 08.45.22.png](https://bitbucket.org/repo/4pGgEAR/images/4263591164-Simulator%20Screen%20Shot%20-%20iPhone%20X%20-%202018-11-22%20at%2008.45.22.png)ß