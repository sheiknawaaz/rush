import * as ActionTypes from "../actions/actionTypes";
const initialState = {
  longitude: 0,
  latitude: 0,
  error: "",
  available: false
};

export const currentLocation = (state = initialState, action) => {
  switch (action.type) {
    case ActionTypes.UPDATE_CURRENT_LOCATION:
      return {
        ...state,
        longitude: action.longitude,
        latitude: action.latitude,
        available: true,
        error: ""
      };
    case ActionTypes.LOCATION_ERROR:
      return {
        ...state,
        available: false,
        error: action.error
      };
    default:
      return state;
  }
};
