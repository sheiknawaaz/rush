import * as ActionTypes from "../actions/actionTypes";

const initialState = {
  lastAddress: "",
  lastAddTime: 0,
  geocoding: false,
  error: "",
  list: []
};

export const addressList = (state = initialState, action) => {
  switch (action.type) {
    case ActionTypes.START_SOLVE:
      return {
        ...state,
        error: ""
      };
    case ActionTypes.START_GEOCODING:
      return {
        ...state,
        geocoding: true,
        error: ""
      };
    case ActionTypes.STOP_GEOCODING:
      return {
        ...state,
        geocoding: false,
        error: action.errMsg
      };
    case ActionTypes.ADD_ADDRESS:
      return {
        ...state,
        list: action.list,
        lastAddress: action.address,
        lastAddTime: action.time
      };
    case ActionTypes.REMOVE_ADDRESS:
      return {
        ...state,
        list: action.list
      };
    case ActionTypes.CLEAR_ERROR:
      return {
        ...state,
        error: ""
      };
    case ActionTypes.CLEAR_ADDRESSES:
      return {
        ...state,
        list: []
      };
    default:
      return state;
  }
};
