import * as ActionTypes from "../actions/actionTypes";

const initialState = {
  solving: false,
  error: "",
  list: []
};

export const solution = (state = initialState, action) => {
  switch (action.type) {
    case ActionTypes.START_SOLVE:
      return {
        ...state,
        solving: true,
        error: ""
      };
    case ActionTypes.STOP_SOLVE:
      return {
        ...state,
        solving: false,
        list: action.list,
        error: action.errMsg
      };
    case ActionTypes.CLEAR_SOLUTION_ERROR:
      return {
        ...state,
        error: ""
      };
    default:
      return state;
  }
};
