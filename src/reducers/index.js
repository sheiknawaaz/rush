import { combineReducers } from "redux";

import { network } from "./network";
import { currentLocation } from "./currentLocation";
import { addressList } from "./addressList";
import { solution } from "./solution";

const AppReducer = combineReducers({
  network,
  currentLocation,
  addressList,
  solution
});

export default AppReducer;
