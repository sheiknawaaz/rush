import * as ActionTypes from "../actions/actionTypes";
const initialState = {
  isConnected: false,
  connectionType: "",
  effectiveType: "",
  mobileData: false
};

export const network = (state = initialState, action) => {
  switch (action.type) {
    case ActionTypes.NETWORK_SET_CONNECTION_TYPE:
      return {
        ...state,
        connectionType: action.connectionType,
        effectiveType: action.effectiveType,
        mobileData: action.mobileData,
        isConnected: action.isConnected
      };
    default:
      return state;
  }
};
