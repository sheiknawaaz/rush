// Network actions
export const NETWORK_SET_CONNECTION_TYPE = "Network_Set_Connection_Type";
export const UPDATE_CURRENT_LOCATION = "Update_Current_Location";
export const LOCATION_ERROR = "Location_Error";
export const START_GEOCODING = "Start_Geocoding";
export const STOP_GEOCODING = "End_Geocoding";
export const ADD_ADDRESS = "Add_Address";
export const REMOVE_ADDRESS = "Remove_Address";
export const CLEAR_ERROR = "Clear_Error";
export const CLEAR_ADDRESSES = "Clear_Address_List";
export const START_SOLVE = "Start_Solve";
export const STOP_SOLVE = "End_Solve";
export const CLEAR_SOLUTION_ERROR = "Clear_Solution_Error";
