import * as ActionTypes from "./actionTypes";
import Config from "react-native-config";

export const clearSolutionError = () => {
  return function(dispatch, getState) {
    dispatch({
      type: ActionTypes.CLEAR_SOLUTION_ERROR
    });
  };
};

function getCombinations(list) {
  let result = [];
  let length = list.length;
  for (let i = 0; i < length; i++) {
    let pair1 = Object.assign({}, list[i]);
    for (let j = i + 1; j < length; j++) {
      let pair2 = Object.assign({}, list[j]);
      result.push({ source: i, dest: j, elems: [pair1, pair2] });
    }
  }
  return result;
}
/*
function getCombinations(list) {
  if (list.length < 2) {
    return [];
  }
  var first = list[0],
    rest = list.slice(1),
    pairs = rest.map(function(x) {
      return [first, x];
    });
  return pairs.concat(getCombinations(rest));
}*/

function generatePermutations(Arr) {
  var permutations = [];
  var A = Arr.slice();

  function swap(a, b) {
    var tmp = A[a];
    A[a] = A[b];
    A[b] = tmp;
  }

  function generate(n, A) {
    if (n == 1) {
      permutations.push(A.slice());
    } else {
      for (var i = 0; i <= n - 1; i++) {
        generate(n - 1, A);
        swap(n % 2 == 0 ? i : 0, n - 1);
      }
    }
  }
  generate(A.length, A);
  return permutations;
}

function generateRoutes(index) {
  var pems = generatePermutations(index.slice(1));
  for (var i = 0; i < pems.length; i++) {
    pems[i].unshift(index[0]);
    pems[i].push(index[0]);
  }
  return pems;
}

async function getDistanceMatrix(combinations) {
  let origins = "";
  let destinations = "";

  combinations.forEach((item, index) => {
    let source = item.elems[0];
    let destination = item.elems[1];
    if (origins !== "") origins = origins + "|";
    origins = origins + source.latitude + "," + source.longitude;
    if (destinations !== "") destinations = destinations + "|";
    destinations =
      destinations + destination.latitude + "," + destination.longitude;
  });

  // for each combination, get a distance (in km) and optionally time (incase we wish to solve by quickest, not nearest)
  let url =
    Config.GOOGLE_DISTANCE_URL +
    "?key=" +
    Config.GOOGLE_MAPS_API_KEY +
    encodeURI(`&origins=${origins}&destinations=${destinations}`);
  const response = await fetch(url).catch(error => {
    return Promise.reject(new Error("Error fetching data"));
  });

  const json = await response.json().catch(error => {
    return Promise.reject(new Error("Error parsing server response"));
  });

  let distances = [];
  if (json.status === "OK" && json.rows.length > 0) {
    let distances = [];
    let resp = json.rows;
    let results = resp[0].elements;
    distances = combinations.map((item, index) => {
      let distanceResp = resp[index].elements[index];
      let newItem = Object.assign({}, item);
      newItem.distance = distanceResp.distance.value / 1000; // (data is returned in metres, so convert to kms)
      newItem.distanceText = distanceResp.distance.text;
      newItem.duration = distanceResp.duration.value;
      newItem.durationText = distanceResp.duration.text;
      return newItem;
    });
    return distances;
  } else
    throw new Error(
      "Error getting distance matrix from Google Maps API. Server returned " +
        json.status
    );
}

function getWeightInfo(source, dest, matrix) {
  for (let k = 0; k < matrix.length; k++) {
    let item = matrix[k];
    if (
      (item.source == source && item.dest == dest) ||
      (item.dest == source && item.source == dest)
    ) {
      return {
        distance: item.distance,
        duration: item.duration,
        durationText: item.durationText
      };
    }
  }
  return 0;
}

export const solve = () => {
  return async (dispatch, getState) => {
    let isConnected = getState().network.isConnected;

    if (!isConnected) {
      dispatch({
        type: ActionTypes.STOP_SOLVE,
        errMsg: "No Internet connection"
      });
      return;
    }
    dispatch({ type: ActionTypes.START_SOLVE });
    try {
      let list = getState().addressList.list;
      let combinations = getCombinations(list); // paired combinations which we'll get distances for

      let permutations = generateRoutes(
        list.map((item, index) => {
          return index;
        })
      ); // permutations = all possible route combinations that start with first element and end at first elements

      let distances = await getDistanceMatrix(combinations);
      // we now have a list of distances for each possible combination
      // next, calculate the weight of each route in permutations by summing from info in distance matrix

      let weights = [];
      permutations.forEach((item, index) => {
        let sum = 0;
        for (i = 1; i < item.length; i++) {
          let sourceIndex = item[i - 1];
          let destIndex = item[i];
          let weightInfo = getWeightInfo(sourceIndex, destIndex, distances);
          let weight = weightInfo.distance;
          sum = sum + weight;
        }
        weights.push({
          weight: sum,
          list: item
        });
      });

      // sort by total weight so that optimal solution is first
      weights.sort((a, b) => {
        return a.weight - b.weight;
      });

      // the optimal route will now be the first element in weights
      let optimalIndex = weights[0].list;
      let optimalRoute = [];
      optimalIndex.forEach((item, index) => {
        optimalRoute.push(Object.assign({}, list[item]));
      });

      // add distance and time info back into the optimalRoute for displaying in solutions list
      optimalRoute[0].distance = 0;
      optimalRoute[0].travelTime = "";
      for (let i = 1; i < optimalRoute.length; i++) {
        let source = optimalIndex[i - 1];
        let dest = optimalIndex[i];
        let weightInfo = getWeightInfo(source, dest, distances);
        optimalRoute[i].distance = weightInfo.distance;
        optimalRoute[i].travelTime = weightInfo.durationText;
      }
      let newList = optimalRoute; //Object.assign([], list);
      dispatch({ type: ActionTypes.STOP_SOLVE, list: newList, errMsg: "" });
    } catch (error) {
      dispatch({
        type: ActionTypes.STOP_GEOCODING,
        list: [],
        errMsg: error.message
      });
    }
  };
};
