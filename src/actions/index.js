import * as Network from "./network";
import * as CurrentLocation from "./currentLocation";
import * as AddressList from "./addressList";
import * as Solver from "./solution";

export const ActionCreators = Object.assign(
  {},
  Network,
  CurrentLocation,
  AddressList,
  Solver
);
