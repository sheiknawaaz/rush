import * as ActionTypes from "./actionTypes";
import Config from "react-native-config";
//import PushNotification from "react-native-push-notification";
import { pushNotifications } from "../services";

async function doAutoCompleteLookup(partAddress) {
  // autocomplete lookup gets us a nicely formatted address broken up into line1 and line2
  let url =
    Config.GOOGLE_AUTOCOMPLETE_URL +
    "?key=" +
    Config.GOOGLE_MAPS_API_KEY +
    "&input=" +
    encodeURI(partAddress);
  const response = await fetch(url);
  const json = await response.json();
  if (json.status === "OK" && json.predictions.length > 0) {
    let structured = json.predictions[0].structured_formatting;
    return {
      line1: structured.main_text,
      line2: structured.secondary_text
    };
  } else
    return {
      line1: partAddress,
      line2: ""
    };
}

export const checkDuplicateAddress = (formattedAddress, list) => {
  for (let i = 0; i < list.length; i++) {
    if (list[i].formattedAddress == formattedAddress)
      throw new Error(`Address ${formattedAddress} is already in the list`);
  }
};

export const clearError = () => {
  return function(dispatch, getState) {
    dispatch({ type: ActionTypes.CLEAR_ERROR });
  };
};

export const clearAddressList = () => {
  return function(dispatch, getState) {
    dispatch({ type: ActionTypes.CLEAR_ADDRESSES });
  };
};

export const removeAddress = index => {
  return async (dispatch, getState) => {
    let newList = Object.assign([], getState().addressList.list);
    newList.splice(index, 1);
    dispatch({
      type: ActionTypes.REMOVE_ADDRESS,
      list: newList
    });
  };
};

export const addAddressInfo = (formattedAddress, line1, line2, lat, lng) => {
  return function(dispatch, getState) {
    let newList = Object.assign([], getState().addressList.list);
    checkDuplicateAddress(formattedAddress, newList);
    let newAddress = {
      formattedAddress: formattedAddress,
      address1: line1,
      address2: line2,
      latitude: lat,
      longitude: lng
    };
    newList.push(newAddress);
    let timeAdded = new Date();
    dispatch({
      type: ActionTypes.ADD_ADDRESS,
      list: newList,
      address: formattedAddress,
      time: timeAdded.getTime()
    });
    //pushNotifications.localNotification("Address added", formattedAddress);
    let timeText = timeAdded.toLocaleTimeString();
    pushNotifications.scheduleNotification(
      `Address added (${timeText})`,
      formattedAddress
    );
  };
};

export const addAddressByCoords = coords => {
  return async (dispatch, getState) => {
    let isConnected = getState().network.isConnected;
    if (!isConnected) return;

    dispatch({ type: ActionTypes.START_GEOCODING });
    try {
      let url =
        Config.GOOGLE_GEOCODING_URL +
        "?key=" +
        Config.GOOGLE_MAPS_API_KEY +
        encodeURI(`&latlng=${coords.latitude},${coords.longitude}`);
      const response = await fetch(url).catch(error => {
        return Promise.reject(new Error("Error fetching data"));
      });

      const json = await response.json().catch(error => {
        return Promise.reject(new Error("Error parsing server response"));
      });

      if (json.status === "OK" && json.results.length > 0) {
        let address = json.results[0].formatted_address;
        let nicelyFormatted = await doAutoCompleteLookup(address);
        dispatch(
          addAddressInfo(
            address,
            nicelyFormatted.line1,
            nicelyFormatted.line2,
            coords.latitude,
            coords.longitude
          )
        );
        dispatch({ type: ActionTypes.STOP_GEOCODING, errMsg: "" });
      } else {
        throw new Error(`Server returned status code ${json.status}`);
      }
    } catch (error) {
      dispatch({ type: ActionTypes.STOP_GEOCODING, errMsg: error.message });
    }
  };
};

export const addAddress = (formattedAddress, line1, line2) => {
  return async (dispatch, getState) => {
    let isConnected = getState().network.isConnected;

    if (!isConnected) {
      dispatch({
        type: ActionTypes.STOP_GEOCODING,
        errMsg: "No Internet connection"
      });
      return;
    }
    dispatch({ type: ActionTypes.START_GEOCODING });
    try {
      checkDuplicateAddress(formattedAddress, getState().addressList.list);
      let url =
        Config.GOOGLE_GEOCODING_URL +
        "?key=" +
        Config.GOOGLE_MAPS_API_KEY +
        "&address=" +
        encodeURI(formattedAddress);
      const response = await fetch(url).catch(error => {
        return Promise.reject(new Error("Error fetching data"));
      });

      const json = await response.json().catch(error => {
        return Promise.reject(new Error("Error parsing server response"));
      });

      let lat = 0;
      let lng = 0;
      if (json.status === "OK" && json.results.length > 0) {
        let location = json.results[0].geometry.location;
        lat = location.lat;
        lng = location.lng;
        //return json;
      } else {
        return Promise.reject(
          new Error(`Server returned status code ${json.status}`)
        );
      }
      dispatch(addAddressInfo(formattedAddress, line1, line2, lat, lng));
      dispatch({ type: ActionTypes.STOP_GEOCODING, errMsg: "" });
    } catch (error) {
      dispatch({ type: ActionTypes.STOP_GEOCODING, errMsg: error.message });
    }
  };
};
