import * as ActionTypes from "./actionTypes";

// update network status
export const setCurrentLocation = locationInfo => {
  return async (dispatch, getState) => {
    dispatch({
      type: ActionTypes.UPDATE_CURRENT_LOCATION,
      latitude: locationInfo.latitude,
      longitude: locationInfo.longitude,
      error: locationInfo.error
    });
  };
};

export const locationError = errMsg => {
  return async (dispatch, getState) => {
    dispatch({
      type: ActionTypes.UPDATE_CURRENT_LOCATION,
      error: errMsg
    });
  };
};
