import * as ActionTypes from "./actionTypes";

// update network status

export const setNetworkConnectionType = connectionInfo => {
  return async (dispatch, getState) => {
    let mobileData = connectionInfo.type == "cellular";
    let isConnected = mobileData || connectionInfo.type == "wifi";
    dispatch({
      type: ActionTypes.NETWORK_SET_CONNECTION_TYPE,
      connectionType: connectionInfo.type,
      effectiveType: connectionInfo.effectiveType,
      mobileData: mobileData,
      isConnected: isConnected
    });
  };
};
