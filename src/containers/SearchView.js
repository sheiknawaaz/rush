import React, { Component } from "react";
import PropTypes from "prop-types";
import { StyleSheet, Text, View, BackHandler } from "react-native";

import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { ActionCreators } from "../actions";

//import {
//  GooglePlacesAutocomplete
//} from "react-native-google-places-autocomplete";

import { GooglePlacesAutocomplete } from "../components/searchview";
import Icon from "react-native-vector-icons/FontAwesome5";
import SearchResult from "./SearchResult";
import Config from "react-native-config";

const ICON_SIZE = 20;

export class SearchView extends Component {
  constructor(props) {
    super(props);
    this.state = {
      showPlacesList: false,
      searchFocused: false
    };
  }

  componentDidMount() {
    let that = this;
    BackHandler.addEventListener("hardwareBackPress", this.onBackButton);
  }

  componentWillUnmount() {
    BackHandler.removeEventListener("hardwareBackPress", this.onBackButton);
  }

  onBackButton = () => {
    if (this.state.showPlacesList) {
      this.setState({
        showPlacesList: false
      });
      return true; // disable backButton
    } else if (this.state.searchFocused) {
      this.googlePlacesAutocomplete._handleChangeText("");
      return true;
    } else {
      return false;
    }
  };

  render() {
    return (
      <GooglePlacesAutocomplete
        ref={c => this.googlePlacesAutocomplete = c}
        editable={this.props.editable}
        placeholder="Search here"
        minLength={2} // minimum length of text to search
        autoFocus={false}
        returnKeyType={"search"} // Can be left out for default return key https://facebook.github.io/react-native/docs/textinput.html#returnkeytype
        listViewDisplayed={this.state.showPlacesList} // true/false/auto/undefined
        enablePoweredByContainer={false}
        fetchDetails={true}
        renderDescription={row => row.description} // custom description render
        onPress={(data, details = null) => {
          // 'details' is provided when fetchDetails = true
          this.props.actions.addAddress(
            data.description,
            data.structured_formatting.main_text,
            data.structured_formatting.secondary_text
          );
        }}
        textInputProps={{
          onFocus: () =>
            this.setState({ searchFocused: true, showPlacesList: true }),
          onBlur: () =>
            this.setState({ searchFocused: false, showPlacesList: false })
        }}
        renderRow={rowData => {
          return (
            <SearchResult
              rowData={rowData}
              onAddAddress={this.props.actions.addAddress}
              onAfterAdd={() => {
                this.setState({ showPlacesList: false });
              }}
            />
          );
        }}
        renderLeftButton={() => (
          <Icon
            style={{
              position: "absolute",
              alignSelf: "center",
              paddingLeft: 10,
              color: "grey"
            }}
            size={20}
            name={"search"}
          />
        )}
        getDefaultValue={() => ""}
        suppressDefaultStyles={true}
        query={{
          // available options: https://developers.google.com/places/web-service/autocomplete
          key: Config.GOOGLE_MAPS_API_KEY, //"AIzaSyAwqIhTudJSfvo_cgr3abQNr4Z8xo6JmIM",
          language: "en" // language of the results
        }}
        styles={{
          textInputContainer: {
            flexDirection: "row",
            justifyContents: "center",
            alignItems: "center",
            width: "100%",
            backgroundColor: "white",
            height: null
          },
          textInput: {
            textAlign: "center",
            height: 50,
            width: "100%",
            fontSize: 16,
            paddingLeft: 35,
            paddingRight: 5
          },
          listView: {
            backgroundColor: "white",
            borderRadius: 5
          },
          separator: {
            height: 0.5,
            backgroundColor: "#c8c7cc"
          }
        }}
        currentLocation={false} // Will add a 'Current location' button at the top of the predefined places list
        nearbyPlacesAPI="GooglePlacesSearch" // Which API to use: GoogleReverseGeocoding or GooglePlacesSearch
        //nearbyPlaceAPI={"GoogleReverseGeocoding"}
        GoogleReverseGeocodingQuery={{
          // available options for GoogleReverseGeocoding API : https://developers.google.com/maps/documentation/geocoding/intro
        }}
        GooglePlacesSearchQuery={{
          // available options for GooglePlacesSearch API : https://developers.google.com/places/web-service/search
          rankby: "distance"
        }}
        filterReverseGeocodingByTypes={[
          "locality",
          "administrative_area_level_3"
        ]} // filter the reverse geocoding results by types - ['locality', 'administrative_area_level_3'] if you want to display only cities
        debounce={200} // debounce the requests in ms. Set to 0 to remove debounce. By default 0ms.
      />
    );
  }
}

const mapStateToProps = state => ({
  network: state.network,
  currentLocation: state.currentLocation
});

function mapDispatchToProps(dispatch) {
  return {
    actions: bindActionCreators(ActionCreators, dispatch),
    dispatch
  };
}
export default connect(mapStateToProps, mapDispatchToProps)(SearchView);
