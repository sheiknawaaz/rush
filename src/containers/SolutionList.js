import React, { Component } from "react";
import {
  StyleSheet,
  Text,
  View,
  FlatList,
  ActivityIndicator
} from "react-native";

import { Divider } from "react-native-paper";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { ActionCreators } from "../actions";
import * as GlobalColors from "../../res/colors";

export class SolutionList extends Component {
  constructor(props) {
    super(props);
  }

  renderSeparator = () => {
    return <Divider />;
  };

  renderItem = (item, index) => {
    const renderDistance = distance => {
      if (distance == 0 && item.travelTime == "") return null;
      let distanceText = distance === 0
        ? ""
        : "Distance: " + distance.toFixed(2) + "km";
      let timeText =
        (distanceText !== "" ? " " : "") + "Travel Time: " + item.travelTime;
      return (
        <Text style={styles.secondaryAddress}>
          {distanceText + timeText}
        </Text>
      );
    };
    let { actions } = this.props;
    let lineNo = index + 1;
    return (
      <View style={styles.itemContainer}>
        <View style={styles.indexContainer}>
          <Text style={styles.lineNo}>({lineNo.toString()})</Text>
        </View>
        <View style={styles.addressContainer}>
          <Text style={styles.mainAddress}>
            {item.address1}
          </Text>
          <Text style={styles.secondaryAddress}>
            {item.address2}
          </Text>
          {renderDistance(item.distance)}
        </View>
      </View>
    );
  };

  renderLoading = () => {
    return (
      <View style={styles.busyContainer}>
        <ActivityIndicator size="large" color={GlobalColors.PRIMARY} />
      </View>
    );
  };

  render() {
    let { list, loading } = this.props;
    if (loading) return this.renderLoading();
    return (
      <View style={styles.container}>
        <FlatList
          data={list}
          renderItem={({ item, index }) => this.renderItem(item, index)}
          ItemSeparatorComponent={this.renderSeparator}
          keyExtractor={(item, index) => {
            return index.toString();
          }}
        />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: "white",
    flex: 1
  },
  busyContainer: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center"
  },
  itemContainer: {
    flexDirection: "row",
    height: 70,
    alignItems: "center",
    justifyContent: "flex-start"
  },
  indexContainer: {
    justifyContent: "center",
    alignItems: "center",
    width: 40
  },
  lineNo: {
    fontSize: 18,
    fontWeight: "600"
  },
  addressContainer: {
    flexDirection: "column"
  },
  mainAddress: {
    fontWeight: "600",
    fontSize: 16
  },
  secondaryAddress: {
    fontSize: 14,
    color: "grey"
  }
});

const mapStateToProps = state => ({
  list: state.solution.list,
  loading: state.solution.solving,
  loadError: state.solution.error
});

function mapDispatchToProps(dispatch) {
  return {
    actions: bindActionCreators(ActionCreators, dispatch),
    dispatch
  };
}
export default connect(mapStateToProps, mapDispatchToProps)(SolutionList);
