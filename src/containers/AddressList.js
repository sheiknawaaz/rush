import React, { Component } from "react";
import {
  StyleSheet,
  Text,
  View,
  FlatList,
  ActivityIndicator
} from "react-native";

import { Divider } from "react-native-paper";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { ActionCreators } from "../actions";
import * as GlobalColors from "../../res/colors";
import Swipeable from "react-native-swipeable";
import { IconButton } from "react-native-paper";

export class AddressList extends Component {
  constructor(props) {
    super(props);
  }

  renderSeparator = () => {
    return <Divider />;
  };

  renderItem = (item, index) => {
    let { actions } = this.props;
    let lineNo = index + 1;
    const leftContent = <Text>Pull to activate</Text>;
    const leftButton = (
      <View style={styles.leftSwipeItem}>
        <IconButton
          icon="remove"
          color={GlobalColors.SECONDARY}
          size={20}
          onPress={() => {
            actions.removeAddress(index);
          }}
        />
      </View>
    );
    return (
      <Swipeable leftButtons={[leftButton]}>
        <View style={styles.itemContainer}>
          <View style={styles.indexContainer}>
            <Text style={styles.lineNo}>({lineNo.toString()})</Text>
          </View>
          <View style={styles.addressContainer}>
            <Text style={styles.mainAddress}>
              {item.address1}
            </Text>
            <Text style={styles.secondaryAddress}>
              {item.address2}
            </Text>
          </View>
        </View>
      </Swipeable>
    );
  };

  renderLoading = () => {
    return (
      <View style={styles.busyContainer}>
        <ActivityIndicator size="large" color={GlobalColors.PRIMARY} />
      </View>
    );
  };

  render() {
    let { list, loading } = this.props;
    if (loading) return this.renderLoading();
    return (
      <View style={styles.container}>
        <FlatList
          data={list}
          renderItem={({ item, index }) => this.renderItem(item, index)}
          ItemSeparatorComponent={this.renderSeparator}
          keyExtractor={(item, index) => {
            return index.toString();
          }}
        />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: "white",
    flex: 1
  },
  busyContainer: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center"
  },
  itemContainer: {
    flexDirection: "row",
    height: 60,
    alignItems: "center",
    justifyContent: "flex-start"
  },
  indexContainer: {
    justifyContent: "center",
    alignItems: "center",
    width: 40
  },
  lineNo: {
    fontSize: 18,
    fontWeight: "600"
  },
  addressContainer: {
    flexDirection: "column"
  },
  mainAddress: {
    fontWeight: "600",
    fontSize: 16
  },
  secondaryAddress: {
    fontSize: 14,
    color: "grey"
  },
  leftSwipeItem: {
    flex: 1,
    alignItems: "flex-end",
    justifyContent: "center",
    paddingRight: 20,
    backgroundColor: "red"
  }
});

const mapStateToProps = state => ({
  list: state.addressList.list,
  loading: state.addressList.geocoding,
  loadError: state.addressList.error
});

function mapDispatchToProps(dispatch) {
  return {
    actions: bindActionCreators(ActionCreators, dispatch),
    dispatch
  };
}
export default connect(mapStateToProps, mapDispatchToProps)(AddressList);
