import React, { Component } from "react";
import { NetInfo, StatusBar, InteractionManager } from "react-native";

import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { ActionCreators } from "../actions";

import { SafeAreaView } from "react-navigation";
import AppWithNavigationState from "../navigators/AppNavigator";
import { Provider as PaperProvider } from "react-native-paper";

import * as Permissions from "../components/permissions";
import * as GlobalColors from "../../res/colors";

class AppContainer extends Component {
  constructor(props) {
    super(props);
  }

  componentDidMount() {
    // get initial
    NetInfo.getConnectionInfo().then(connectionInfo => {
      this.handleConnectionInfo(connectionInfo);
    });
    // watch for changes
    NetInfo.addEventListener("connectionChange", this.handleConnectionInfo);

    InteractionManager.runAfterInteractions(() => {
      Permissions.requestLocationPermission()
        .then(granted => {
          if (granted) this.monitorLocationChanges();
          else
            this.props.actions.locationError(
              "Location permission was not granted"
            );
        })
        .done();
    });
  }

  componentWillUnmount() {
    navigator.geolocation.clearWatch(this.watchId);
    NetInfo.removeEventListener("connectionChange", this.handleConnectionInfo);
  }

  monitorLocationChanges = () => {
    // watch for location changes
    this.watchId = navigator.geolocation.watchPosition(
      position => {
        this.handleLocationUpdate({
          latitude: position.coords.latitude,
          longitude: position.coords.longitude,
          error: ""
        });
      },
      error =>
        this.handleLocationUpdate({
          latitude: 0,
          longitude: 0,
          error: error.message
        }),
      {
        enableHighAccuracy: true,
        timeout: 20000,
        maximumAge: 1000,
        distanceFilter: 10
      }
    );
  };

  handleLocationUpdate = locationInfo => {
    this.props.actions.setCurrentLocation(locationInfo);
  };

  handleConnectionInfo = connectionInfo => {
    this.props.actions.setNetworkConnectionType(connectionInfo);
  };

  render() {
    return (
      <PaperProvider>
        <StatusBar backgroundColor={GlobalColors.PRIMARY} barStyle="default" />
        <SafeAreaView style={{ flex: 1 }}>
          <AppWithNavigationState />
        </SafeAreaView>
      </PaperProvider>
    );
  }
}

const mapStateToProps = state => ({
  network: state.network
});

function mapDispatchToProps(dispatch) {
  return {
    actions: bindActionCreators(ActionCreators, dispatch),
    dispatch
  };
}
export default connect(mapStateToProps, mapDispatchToProps)(AppContainer);
