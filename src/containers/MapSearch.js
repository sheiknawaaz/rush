import React, { Component } from "react";
import { ActivityIndicator, View, Alert, Text, StyleSheet } from "react-native";

import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { ActionCreators } from "../actions";

import { IconButton, Button } from "react-native-paper";
import MyMapView from "../components/MyMapView";
import SearchView from "./SearchView";
import AddressList from "./AddressList";
import * as GlobalColors from "../../res/colors";

const BUTTON_COLOR = "#263238";

export class MapSearch extends Component {
  constructor(props) {
    super(props);
    this.state = {
      MapHeight: 300,
      MapWidth: 400,
      SelectedCoords: props.currentLocation
    };
  }

  onMapViewLayoutChange = event => {
    // swap height and width on orientation change
    var { x, y, width, height } = event.nativeEvent.layout;

    if (this.state.MapHeight !== height) {
      this.setState({
        MapWidth: width,
        MapHeight: height
      });
    }
  };

  locationSelected = coordinates => {
    this.setState({ SelectedCoords: coordinates });
  };

  renderMap = () => {
    return (
      <View style={styles.mapContainer} onLayout={this.onMapViewLayoutChange}>
        <MyMapView
          height={this.state.MapHeight}
          width={this.state.MapWidth}
          location={this.props.currentLocation}
          draggable={true}
          onSelectLocation={this.locationSelected}
        />
      </View>
    );
  };

  renderFooter = () => {
    return (
      <View style={styles.footer}>
        <Button
          style={styles.button}
          mode="outlined"
          color={BUTTON_COLOR}
          onPress={() => {
            this.props.navigation.goBack();
          }}
        >
          Cancel
        </Button>
        <Button
          style={styles.button}
          mode="outlined"
          color={BUTTON_COLOR}
          onPress={() => {
            this.props.actions.addAddressByCoords(this.state.SelectedCoords);
            this.props.navigation.goBack();
          }}
        >
          Confirm
        </Button>
      </View>
    );
  };

  render() {
    return (
      <View style={styles.container}>
        {this.renderMap()}
        {this.renderFooter()}
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: "column",
    backgroundColor: 1
  },
  mapContainer: {
    flex: 1,
    borderWidth: 1
  },
  footer: {
    position: "absolute",
    height: 60,
    width: "100%",
    bottom: 0,
    padding: 10,
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "space-evenly",
    backgroundColor: "white"
  },
  button: {
    width: 150,
    borderRadius: 20,
    paddingHorizontal: 10,
    backgroundColor: "#E0E0E0"
  }
});

const mapStateToProps = state => ({
  network: state.network,
  currentLocation: state.currentLocation
});

function mapDispatchToProps(dispatch) {
  return {
    actions: bindActionCreators(ActionCreators, dispatch),
    dispatch
  };
}
export default connect(mapStateToProps, mapDispatchToProps)(MapSearch);
