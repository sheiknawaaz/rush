import React, { Component } from "react";
import {
  ActivityIndicator,
  View,
  Alert,
  Text,
  StyleSheet,
  InteractionManager
} from "react-native";

import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { ActionCreators } from "../actions";

import { IconButton, Button, Snackbar } from "react-native-paper";
import MyMapView from "../components/MyMapView";
import SearchView from "./SearchView";
import AddressList from "./AddressList";
import * as GlobalColors from "../../res/colors";
import Config from "react-native-config";

export class MainScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      MapHeight: 300,
      MapWidth: 400,
      initialised: false,
      displayAppConfig: false
    };
  }

  componentDidMount() {
    InteractionManager.runAfterInteractions(() => {
      this.setState({
        displayAppConfig: true
      });
    });
  }

  componentDidUpdate(prevProps) {
    let { errorMsg } = this.props;
    if (errorMsg !== "" && errorMsg != prevProps.errorMsg)
      this.showErrorMsg(errorMsg);
  }

  displaySnackbar = () => {
    return (
      <Snackbar
        visible={this.state.displayAppConfig}
        onDismiss={() => this.setState({ displayAppConfig: false })}
      >
        Running in {Config.MODE} mode
      </Snackbar>
    );
  };

  onMapViewLayoutChange = event => {
    // swap height and width on orientation change
    var { x, y, width, height } = event.nativeEvent.layout;
    if (this.state.MapHeight !== height) {
      this.setState({
        MapWidth: width,
        MapHeight: height,
        initialised: true
      });
    }
  };

  showErrorMsg = errorMsg => {
    const { actions } = this.props;
    Alert.alert("Error", errorMsg, [
      {
        text: "OK",
        onPress: () => {
          actions.clearError();
        }
      }
    ]);
  };

  renderLoading = () => {
    return <ActivityIndicator size="large" color="#00ff00" />;
  };

  renderMap = () => {
    if (!this.state.initialised) return null;
    if (this.props.errorMsg !== "") return null; // optimization, any error dialogs get really really slow if the map is displayed underneath
    if (this.props.currentLocation && this.props.currentLocation.available)
      return (
        <MyMapView
          height={this.state.MapHeight}
          width={this.state.MapWidth}
          location={this.props.currentLocation}
          addresses={this.props.addressList}
        />
      );
    else return this.renderLoading();
  };

  renderCurrentLocation = () => {
    return (
      <View style={styles.currentLocationContainer}>
        <IconButton
          icon="my-location"
          color={"white"}
          size={30}
          onPress={() => {
            this.props.navigation.navigate("MapSearch");
          }}
        />
      </View>
    );
  };

  renderSearch = () => {
    let editable =
      this.props.currentLocation && this.props.currentLocation.available;
    return (
      <View style={styles.searchContainer}>
        <SearchView editable={editable} />
      </View>
    );
  };

  renderListHeader = () => {
    return (
      <View style={styles.listHeader}>
        <View style={{ flex: 0.5, flexDirection: "row", alignItems: "center" }}>
          <Text style={styles.listHeaderText}>Directions</Text>
          <IconButton
            icon="clear-all"
            disabled={this.props.addressList.length == 0}
            color={GlobalColors.PRIMARY}
            style={{ paddingLeft: 5 }}
            size={26}
            onPress={() => {
              Alert.alert(
                "Confirm Delete All",
                "Are you sure you wish to clear the address list?",
                [
                  {
                    text: "No",
                    onPress: () => console.log("cancelled"),
                    style: "cancel"
                  },
                  {
                    text: "Yes",
                    onPress: () => {
                      this.props.actions.clearAddressList();
                    }
                  }
                ]
              );
            }}
          />
        </View>
        <View style={{ flex: 0.5, paddingRight: 10 }}>
          <View style={styles.directionButtonContainer}>
            <Button
              icon="directions"
              mode="contained"
              color={GlobalColors.PRIMARY}
              onPress={() => {
                if (this.props.addressList.length < 2)
                  this.showErrorMsg(
                    "You need atleast 2 addresses to use this tool"
                  );
                else {
                  this.props.actions.solve();
                  this.props.navigation.navigate("Solution");
                }
              }}
            >
              Directions
            </Button>
          </View>
        </View>
      </View>
    );
  };
  renderList = () => {
    return <AddressList />;
  };

  render() {
    return (
      <View style={{ flex: 1, flexDirection: "column" }}>
        <View
          onLayout={this.onMapViewLayoutChange}
          style={{ flex: 0.5, justifyContent: "center", alignItems: "center" }}
        >
          {this.renderMap()}
          {this.renderCurrentLocation()}
        </View>
        <View style={{ flex: 0.5 }}>
          {this.renderListHeader()}
          {this.renderList()}
        </View>
        {this.renderSearch()}
        {this.displaySnackbar()}
      </View>
    );
  }
}

const styles = StyleSheet.create({
  currentLocationContainer: {
    position: "absolute",
    bottom: 20,
    right: 20,
    width: 40,
    height: 40,
    borderRadius: 15,
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "rgba(0,0,0,0.5)"
  },
  searchContainer: {
    position: "absolute",
    top: 20,
    left: 10,
    right: 10,
    borderWidth: 1,
    borderColor: GlobalColors.BORDER
  },
  listHeader: {
    height: 50,
    flexDirection: "row",
    alignItems: "center",
    backgroundColor: "white",
    borderTopWidth: 1,
    borderBottomWidth: 1,
    borderColor: GlobalColors.BORDER
  },
  listHeaderText: {
    fontSize: 18,
    fontWeight: "600",
    paddingLeft: 10
  },
  directionButtonContainer: {
    alignSelf: "flex-end",
    width: 150
  }
});

const mapStateToProps = state => ({
  network: state.network,
  currentLocation: state.currentLocation,
  addressList: state.addressList.list,
  errorMsg: state.addressList.error
});

function mapDispatchToProps(dispatch) {
  return {
    actions: bindActionCreators(ActionCreators, dispatch),
    dispatch
  };
}
export default connect(mapStateToProps, mapDispatchToProps)(MainScreen);
