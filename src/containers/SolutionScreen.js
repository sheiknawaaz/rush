import React, { Component } from "react";
import {
  ActivityIndicator,
  View,
  Alert,
  Text,
  StyleSheet,
  InteractionManager
} from "react-native";

import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { ActionCreators } from "../actions";

import { IconButton, Button } from "react-native-paper";
import MyMapView from "../components/MyMapView";
import SolutionList from "./SolutionList";
import * as GlobalColors from "../../res/colors";

export class SolutionScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      MapHeight: 300,
      MapWidth: 400,
      initialised: false
    };
  }

  componentDidUpdate(prevProps) {
    let { errorMsg } = this.props;
    if (errorMsg !== "" && errorMsg != prevProps.errorMsg)
      this.showErrorMsg(errorMsg);
  }

  onMapViewLayoutChange = event => {
    // swap height and width on orientation change
    var { x, y, width, height } = event.nativeEvent.layout;
    if (this.state.MapHeight !== height) {
      this.setState({
        MapWidth: width,
        MapHeight: height,
        initialised: true
      });
    }
  };

  showErrorMsg = errorMsg => {
    const { actions } = this.props;
    Alert.alert("Error", errorMsg, [
      {
        text: "OK",
        onPress: () => {
          actions.clearSolutionError();
        }
      }
    ]);
  };

  renderLoading = () => {
    return <ActivityIndicator size="large" color="#00ff00" />;
  };

  renderMap = () => {
    if (!this.state.initialised) return null;
    return (
      <MyMapView
        height={this.state.MapHeight}
        width={this.state.MapWidth}
        location={this.props.currentLocation}
        addresses={this.props.list}
        connectAddresses={true}
      />
    );
  };

  renderListHeader = () => {
    return (
      <View style={styles.listHeader}>
        <View style={{ flex: 0.5 }}>
          <Text style={styles.listHeaderText}>Solution</Text>
        </View>
        <View style={{ flex: 0.5, paddingRight: 10 }}>
          <View style={styles.homeButtonContainer}>
            <Button
              icon="home"
              mode="contained"
              color={GlobalColors.PRIMARY}
              onPress={() => {
                this.props.navigation.goBack();
              }}
            >
              Start Over
            </Button>
          </View>
        </View>
      </View>
    );
  };
  renderList = () => {
    return <SolutionList />;
  };

  render() {
    if (this.props.solving) {
      return (
        <View
          style={{ flex: 1, justifyContent: "center", alignItems: "center" }}
        >
          {this.renderLoading()}
        </View>
      );
    }
    return (
      <View style={{ flex: 1, flexDirection: "column" }}>
        <View
          onLayout={this.onMapViewLayoutChange}
          style={{ flex: 0.5, justifyContent: "center", alignItems: "center" }}
        >
          {this.renderMap()}
        </View>
        <View style={{ flex: 0.5 }}>
          {this.renderListHeader()}
          {this.renderList()}
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  listHeader: {
    height: 50,
    flexDirection: "row",
    alignItems: "center",
    backgroundColor: "white",
    borderTopWidth: 1,
    borderBottomWidth: 1,
    borderColor: GlobalColors.BORDER
  },
  listHeaderText: {
    fontSize: 18,
    fontWeight: "600",
    paddingLeft: 10
  },
  homeButtonContainer: {
    alignSelf: "flex-end",
    width: 150
  }
});

const mapStateToProps = state => ({
  network: state.network,
  currentLocation: state.currentLocation,
  list: state.solution.list,
  solving: state.solution.solving,
  errorMsg: state.solution.error
});

function mapDispatchToProps(dispatch) {
  return {
    actions: bindActionCreators(ActionCreators, dispatch),
    dispatch
  };
}
export default connect(mapStateToProps, mapDispatchToProps)(SolutionScreen);
