import React, { Component } from "react";
import PropTypes from "prop-types";
import { StyleSheet, Text, View, Image } from "react-native";

import { IconButton } from "react-native-paper";
import * as GlobalColors from "../../res/colors";

import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { ActionCreators } from "../actions";

const SearchResult = ({ rowData, onAddAddress, onAfterAdd }) => {
  let fallbackAddress =
    rowData.description || rowData.formatted_address || rowData.name;
  let line1 = fallbackAddress;
  let line2 = "";
  if (rowData.structured_formatting) {
    line1 = rowData.structured_formatting.main_text;
    if (rowData.structured_formatting.secondary_text)
      line2 = rowData.structured_formatting.secondary_text;
    line2 = line2.trim();
  }

  const renderButton = () => {
    return (
      <View style={styles.buttonContainer}>
        <IconButton
          icon="add-circle"
          color={GlobalColors.PRIMARY}
          size={20}
          onPress={() => {
            onAddAddress(fallbackAddress, line1, line2);
            onAfterAdd();
          }}
        />
      </View>
    );
  };

  return (
    <View style={styles.container}>
      {renderButton()}
      <View style={styles.addressContainer}>
        <Text style={styles.mainAddress}>
          {line1}
        </Text>
        <Text style={styles.secondaryAddress}>
          {line2}
        </Text>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flexDirection: "row",
    height: 50,
    alignItems: "center",
    justifyContent: "flex-start"
  },
  buttonContainer: {
    justifyContent: "center",
    alignItems: "center",
    width: 40
  },
  addressContainer: {
    flexDirection: "column"
  },
  mainAddress: {
    fontWeight: "600",
    fontSize: 14
  },
  secondaryAddress: {
    fontSize: 12,
    color: "grey"
  }
});

const mapStateToProps = state => ({
  network: state.network,
  currentLocation: state.currentLocation
});

function mapDispatchToProps(dispatch) {
  return {
    actions: bindActionCreators(ActionCreators, dispatch),
    dispatch
  };
}
export default connect(mapStateToProps, mapDispatchToProps)(SearchResult);
