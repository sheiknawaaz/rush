import PushNotification from "react-native-push-notification";
import { PushNotificationIOS } from "react-native";

const configure = () => {
  PushNotification.configure({
    onRegister: function(token) {
      //process token
    },

    onNotification: function(notification) {
      // process the notification
      // required on iOS only
      notification.finish(PushNotificationIOS.FetchResult.NoData);
    },

    permissions: {
      alert: true,
      badge: true,
      sound: true
    },

    popInitialNotification: true,
    requestPermissions: true
  });
};

const localNotification = (title, message) => {
  PushNotification.cancelLocalNotifications({ id: "12345" });
  PushNotification.localNotification({
    id: "12345",
    autoCancel: true,
    largeIcon: "ic_launcher",
    smallIcon: "ic_notification",
    title: title,
    message: message,
    playSound: false
  });
};

const scheduleNotification = (title, message) => {
  PushNotification.cancelLocalNotifications({ id: "12345" });
  PushNotification.localNotificationSchedule({
    id: "12345",
    autoCancel: true,
    largeIcon: "ic_launcher",
    smallIcon: "ic_notification",
    title: title,
    message: message,
    playSound: false,
    date: new Date(Date.now() + 60 * 1000 * 5)
  });
};

export { configure, localNotification, scheduleNotification };
