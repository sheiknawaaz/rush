import React, { Component } from "react";
import PropTypes from "prop-types";
import {
  StyleSheet,
  Text,
  View,
  InteractionManager,
  Dimensions
} from "react-native";

import * as GlobalColors from "../../res/colors";
import MapView, { Polyline } from "react-native-maps";

let WITHIN_INDISTANCE = 20; // specified in kms
let DELTA = WITHIN_INDISTANCE / 111.1; // approximately 111.1 km in 1 degree latitude

export class MyMapView extends Component {
  static propTypes = {
    location: PropTypes.object.isRequired
  };

  static defaultProps = {};

  constructor(props) {
    super(props);
    this.state = {
      a: {
        latitude: props.location.latitude,
        longitude: props.location.longitude
      }
    };
  }

  render() {
    let markers = [];
    let polyLines = [];
    let draggable = false;
    if (this.props.draggable) draggable = this.props.draggable;
    let { addresses } = this.props;
    let renderPolyLine = () => null;
    if (addresses) {
      for (let i = 0; i < addresses.length; i++) {
        let address = addresses[i];
        let lineNo = i + 1;
        markers.push(
          <MapView.Marker
            key={i}
            coordinate={{
              latitude: address.latitude,
              longitude: address.longitude
            }}
            title={lineNo.toString()}
            description={address.formattedAddress}
          />
        );
      }
      if (this.props.connectAddresses) {
        renderPolyLine = () => {
          return (
            <Polyline
              coordinates={addresses}
              strokeColor={GlobalColors.PRIMARY} // fallback for when `strokeColors` is not supported by the map-provider
              strokeWidth={6}
            />
          );
        };
      }
    }

    const title = draggable ? `GPS Co-ordinates` : "Current Location";
    const description = draggable
      ? `${this.state.a.latitude} N ${this.state.a.longitude} E`
      : "";

    return (
      <View style={styles.container}>
        <MapView
          style={[
            styles.map,
            {
              width: this.props.width,
              height: this.props.height
            }
          ]}
          initialRegion={{
            latitude: this.state.a.latitude,
            longitude: this.state.a.longitude,
            latitudeDelta: DELTA,
            longitudeDelta: DELTA
          }}
        >
          <MapView.Marker
            coordinate={{
              latitude: this.state.a.latitude,
              longitude: this.state.a.longitude
            }}
            title={title}
            description={description}
            pinColor={GlobalColors.PRIMARY}
            draggable={draggable}
            onDragEnd={e => {
              this.setState({ a: e.nativeEvent.coordinate });
              if (draggable && this.props.onSelectLocation)
                this.props.onSelectLocation(e.nativeEvent.coordinate);
            }}
          />
          {markers}
          {renderPolyLine()}
        </MapView>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center"
  }
});

export default MyMapView;
