"use strict";

import { Platform } from "react-native";

import Permissions from "react-native-permissions";

const PERM_AUTHORIZED = "authorized";
const PERM_DENIED = "denied";
const PERM_UNDETERMINED = "undetermined";
const PERM_LOCATION = "location";

export async function requestLocationPermission() {
  let granted = PERM_DENIED;
  /* response from Permissions.check can be one of the following
          1. authorized - user has authorized the permission
          2. denied - on ios, user will not be prompted again, android, user can be prompted again
          3. restricted - on ios, user can't grant permissions as its blocked by parental controls or device doesn't support it
                        - on android, user has selected never again
          4. undetermined - user has not yet been prompted with a permission dialog
        */

  granted = await Permissions.check(PERM_LOCATION);
  if (granted !== PERM_AUTHORIZED) {
    if (
      granted === PERM_UNDETERMINED ||
      (Platform.OS !== "ios" && (granted = PERM_DENIED))
    ) {
      granted = await Permissions.request(PERM_LOCATION);
    }
  }
  return granted == PERM_AUTHORIZED;
}
