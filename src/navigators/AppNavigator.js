import React, { Component } from "react";
import PropTypes from "prop-types";
import { View, Text, BackHandler, Alert } from "react-native";
import {
  NavigationActions,
  addNavigationHelpers,
  createStackNavigator,
  createSwitchNavigator
} from "react-navigation";

import * as ActionTypes from "../actions/actionTypes";
import { bindActionCreators } from "redux";
import { connect } from "react-redux";
import { transitionConfig } from "./screenTransition";

import MainScreen from "../containers/MainScreen";
import MapSearch from "../containers/MapSearch";
import SolutionScreen from "../containers/SolutionScreen";

export const AppWithNavigationState = createStackNavigator(
  {
    Main: {
      screen: MainScreen
    },
    MapSearch: {
      screen: MapSearch
    },
    Solution: {
      screen: SolutionScreen
    }
  },
  {
    navigationOptions: {
      header: null,
      swipeEnabled: true,
      animationEnabled: true,
      gesturesEnabled: true
    },
    transitionConfig
  }
);

export default AppWithNavigationState;
