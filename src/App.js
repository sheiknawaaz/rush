// main sample adapted from https://github.com/ModusCreateOrg/react-navigation-redux-sample

import React, { Component } from "react";
import { View, Text } from "react-native";
import { Provider } from "react-redux";
import AppContainer from "./containers/AppContainer";
import { store, persistor } from "./store";
import { PersistGate } from "redux-persist/integration/react";
import { pushNotifications } from "./services";

pushNotifications.configure();
const Loading = ({}) => {
  return (
    <View style={{ flex: 1, alignItems: "center", justifyContent: "center" }}>
      <Text>Loading saved data</Text>
    </View>
  );
};

class App extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <Provider store={store}>
        <PersistGate loading={<Loading />} persistor={persistor}>
          <AppContainer />
        </PersistGate>
      </Provider>
    );
  }
}

export default App;
