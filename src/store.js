import { applyMiddleware, createStore, compose } from "redux";
import { persistStore, persistReducer, autoRehydrate } from "redux-persist";
import thunkMiddleware from "redux-thunk";
import { createLogger } from "redux-logger";
import AppReducer from "./reducers";
import { REHYDRATE, PURGE, persistCombineReducers } from "redux-persist";
import storage from "redux-persist/lib/storage"; // or whatever storage you are using
import autoMergeLevel2 from "redux-persist/lib/stateReconciler/autoMergeLevel2";

const persistConfig = {
	key: "root.rush.sp",
	keyPrefix: "",
	timeout: 5000,
	version: 1,
	storage: storage,
	stateReconciler: autoMergeLevel2
};

const persistedReducer = persistReducer(persistConfig, AppReducer);

const loggerMiddleware = createLogger({
	predicate: (getState, action) => __DEV__
}); // set logger to only function in development mode

function configureStore(initialState) {
	// enhancer is a way of composing a bunch of different middleware functions that run through each state transformation
	const enhancer = compose(applyMiddleware(thunkMiddleware, loggerMiddleware));
	return createStore(persistedReducer, initialState, enhancer);
}

export const store = configureStore();
export const persistor = persistStore(store);
